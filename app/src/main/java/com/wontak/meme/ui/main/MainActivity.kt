package com.wontak.meme.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.wontak.meme.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}